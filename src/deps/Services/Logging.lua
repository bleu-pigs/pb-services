------  INITIALIZATION
----  variable declaration
local PBCore = PBCore
local component = component
local Construct = PBCore.ObjectsManager
local Enums = PBCore:depend("EnumsManager").Enums
local Utilities = PBCore:depend("Utilities")
local LuaSignal = PBCore:depend("LuaSignal")
local Scheduler = PBCore:depend("ThreadManager").Scheduler
local wait = Scheduler.wait
local delay = Scheduler.delay
local spawn = Scheduler.spawn
local getTime = os.time
local formatString = string.format
local getTraceback = debug.traceback
local cachedCriticalLogs = {}
------  Declaration
----  Enums
--  LogLevel
--  @desc  All available LogLevels
Construct.new(
    "Enum",
    "EventType",
    {
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4,
        Fatal = 5,
        Security = 6
    }
)
----  Classes
local classdefEntry = Construct.createClass("Entry", Construct.__private.registeredClasses.BaseObject.object) do
    local classPrivate = Construct.__private.registeredClasses.Entry
    local EntryCreatedSignal = Construct.new("LuaSignal", "EntryCreatedSignal")
    root.EntryCreated = EntryCreatedSignal.Signal
    ----  OnCreate
    ----  @param  Enum.EventType EventType, string Message, vararg ...
    function classPrivate:onCreate(EventType, Message, ...)
        Construct.__private.registeredClasses.BaseObject.onCreate(self)
        local selfPrivate = Construct.__private.loadedObjects[self]
        local arguments = {...}
        for i = 1, #arguments do
            arguments[i] = tostring(arguments[i])
        end
        local messageFormatted = formatString(Message, unpack(arguments))
        local entry = {
            message = messageFormatted,
            messageRaw = Message,
            eventType = EventType,
            created = getTime(),
            traceback = getTraceback()
        }
        selfPrivate.entry = entry
        if entry.eventType > 3 then
            cachedCriticalLogs[#cachedCriticalLogs + 1] = self
        end
        EntryCreatedSignal:fire(self)
    end
    ----  API declaration
    --  @param  none
    --  @desc  Returns the recorded entry
    --  @returns  table
    --  @permissions
    --  read:  2 (SERVICE)
    --  write:  4 (NONWRITABLE)
    --  execute: 2 (SERVICE)
    function classdefEntry:getEntryData()
        return Construct.__private.loadedObjects[self].entry
    end
end
----  API definitions
--  @param  Enum.EventType EventType,  string Message,  vararg ...
--  @desc  Creates a new Entry based off EventType
--  @returns  none
function root.record(EventType, Message, ...)
    Construct.new(
        "Entry",
        nil,
        EventType,
        Message,
        ...
    )
end
------  FINALIZE
ready()
spawn(
    function()
        local RemoteStorage = PBCore:depend("RemoteStorage")
        -- TODO: implement 
    end
)